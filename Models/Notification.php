<?php

class Notification extends Eloquent {

    protected $table = 'notifications';
    protected $guarded = array('id');
    
    public static $rules = array(
        "notification_save" => array(
        )
    );

   
    public function School(){
        return $this->belongsTo('School', 'deliver_from_school_id');
    }
    
    public function User(){
        return $this->belongsTo('User', 'deliver_to_user_id');
    }
    
    public function Job(){
        return $this->belongsTo('Job', 'job_id');
    }
    
   // Other methods except relations
     
    public static function validate($data, $form){
        return Validator::make($data, static::$rules[$form]);
    }
    
}
