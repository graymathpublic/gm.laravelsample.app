@extends('layout/school')


@section('content')
@include('includes.sub-header2')

<!--Overlay-->
<div class="Overlay hideOverlay" id="who-available-popup">
	<!--detailPost-->
        <div class="detailPost" style="display:none;">
            <a class="close_popup" href="javascript:void(0);"></a>

	    <div class="jobs_wrap">
 
      </div>
      
    </div>
    <!--detailPost-->

@include('includes.teacher-profile-pop-ups') 
    <!-- User Profile Popup -->
    <div  id="view-teacher-profile-wrap" style="display: none; background: #fff; width: 1090px;margin: 130px auto 20px;position: relative;"> 
          <a class="close_popup" href="javascript:void(0);"></a>
          <div class="Detail_wrap_div" id="view-teacher-profile" >
          </div>
    </div>
    <!-- User Profile Popup -->
</div>
<!--Overlay-->


<div class="container-fluid">
  <div class="container">
    <div class="row main-content"> 
      
      <!-- Whos's Available -->
      <div class="whos-avail-wrap"> 
        
        <!-- open jobs -->
        <div class="pull-left open-jobs">
          <h3>Open Jobs</h3>
          <p>Select Open Jobs below to see available <br>
            teachers.</p>
          <ul class="positionsList">
            <li class="header">
              <div class="Positions pull-left DinBold"><input type="checkbox" name="all_job" class="all-jobs"> Position</div>
              <div class="Availables pull-right DinBold">Status</div>
            </li>
          </ul>
          <ul class="positionsList" id="positionsList">
          </ul>
          <a class="clear-confimed DinBold" id="clear-conform-btn" href="javascript:void(0);" >Clear confirmed</a>
          <a class="clear-confimed DinBold" id="clear-cancel-btn" data-cencel-job="" href="javascript:void(0);" >Cancel</a> </div>
        <!-- open jobs --> 
        
        <!-- right-content -->
        <div class="pull-right who-avail"> 
          
          <!--right-box-->
          <div class="right-content right-box" id="position-header" style="display:none">
            <h3>Available Teachers</h3>
            <div class="clearfix"></div>
            <form class="form-horizontal fav">
              @if($isAgency==true)
              <div class="form-group sch-avail">
                  <label class="col-sm-2 control-label">School</label>
                <a class="school DinBold" href="javascript:void(0);" id="school-title"></a>
              </div>
              @endif
              <div class="form-group">
                  <label class="col-sm-2 control-label">Position</label>
                <a class="position DinBold" href="javascript:void(0);" id="position-title" data-job-id=""></a>
              </div>
            
            <ul class="timingsDiv" style="display:block">
            	<li>
                <div class="leftLi">
                	<div class="titleDiv">Start date:</div>
	                <div class="titleDesc" id="from_date"></div>
              </div>
                <div class="leftLi">
                	<div class="titleDiv">End date:</div>
	                <div class="titleDesc" id="to_date"></div>
              </div>
                  
              <div class="leftLi maginBottom10 maginTop10">
                <div class="titleDiv">Start time:</div>
                <div class="titleDesc" id="start_time"></div>
                <div class="titleDiv">Finish time:</div>
                <div class="titleDesc" id="finish_time"></div>
              </div>
              </li>
            </ul>

              <div class="form-group">
                <label for="firstname" class="col-sm-2 control-label">Filter</label>
                <div class="col-sm-4 fav-dropdown">
                  <div class="dropdown creat-drop">
                      <div class="dropdown-btn"> <div id="display-filter">Taught here before</div> <span class="caret"></span> </div>
                    <ul class="dropdown-menu" id="filter-teacher" style="display: none;">
                      <li><a href="javascript:void(0)" data-sort="all">All</a></li>
                      <li><a href="javascript:void(0)" data-sort="Yes">Taught here before</a></li>
<!--                      <li><a href="javascript:void(0)" data-sort="No">No</a></li>-->
                      <li><a href="javascript:void(0)" data-sort="Favourtie">Favourtie</a></li>
                      <li><a href="javascript:void(0)" data-sort="Rating">Rating</a></li>
                      <li><a href="javascript:void(0)" data-sort="Years of experiance">Years of experience</a></li>
                      <li><a href="javascript:void(0)" data-sort="Distance away">Distance away</a></li>
                      <li><a href="javascript:void(0)" data-sort="Applied for work">Applied for work</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <!--right-box--> 

  <div class="search-div" style="display:none;">
  	<h1>Searching for available teachers...</h1>
    <div class="search-IconDiv"></div>
  </div>
          
  <div class="search-div Search-Available" style="display:none">
  	<div class="search-IconDiv"></div><h1>Searching for available teachers...</h1>
  </div>
          
          <!--right-box-->
          <div id="applied-teachers" class="applied-teachers-div"> </div>
          <!--right-box--> 
          
        </div>
        <!-- right-content -->
        <div class="clearfix"></div>
      </div>
      <!-- Whos's Available --> 
    </div>
  </div>
</div>


@stop