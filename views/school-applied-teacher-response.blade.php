@if(empty($applicants))
	<div class="search-div Search-Available">
            <div class="search-IconDiv"></div><h1>Searching for available teachers...</h1>
        </div>
@else
@foreach( $applicants as $applicant )
  <div class="right-content right-box @if(($applicant['data']->status == 4)||($applicant['data']->status == 2))) disable-request @elseif(($applicant['data']->status == 0)&&($job_status == 1)) disable-request @endif"  id="teacher-{{ $applicant['data']->teacher_id }}"> 
  <!--profile-pic-->
  <div class="profile-pic pull-left">
    @if($applicant['data']->image_id!=NULL)
        <img src="{{ URL::to('download', array($applicant['data']->image_id, true)) }}" alt="{{ $applicant['data']->first_name,' ',$applicant['data']->sir_name}}"/>
    @else
        <img src="assets/images/profile-pic.png" alt=""/>
    @endif
  </div>  
  <!--profile-pic-->
  <div class="tech-basic-detail pull-left"> 
        <!--detail-list-->
        <ul class="detail-list">
      <li>
            <h3>{{ $applicant['data']->first_name,' ',$applicant['data']->sir_name, ' (', $applicant['data']->teacher_id,')' }}</h3>
          </li>
      <li>Taught here before: <span>{{ $applicant['data']->taught_here_before }}</span></li>
      <li>Years teaching: <span>@if($applicant['experience']!=NULL) {{ $applicant['experience'] }} years @else N/A @endif</span></li>
      <li>Distance away: <span>{{ $applicant['distance'] }}</span></li>
      <li>Rating: <span>@if($applicant['data']->rating==''){{ 0 }}@else{{ ceil($applicant['data']->rating) }}@endif/10</span></li>
      <li><a data-flag="{{$applicant['data']->favorite}}" data-school-id="{{ $applicant['data']->school_id }}" data-job-id="{{ $applicant['data']->job_id }}" data-teacher-id="{{ $applicant['data']->teacher_id }}" class="start @if($applicant['data']->favorite=='No')disable @else  active @endif" href="javascript:void(0);" ></a> <a href="javascript:void(0)" data-attr="{{ URL::to('teacher_profile',array($applicant['data']->teacher_id,$applicant['data']->job_id)) }}" class="view-available-teacher-profile-link">View profile</a></li>
    </ul>
        <!--detail-list--> 
      </div>
  <div class="pull-right msg-btn">
  
  @if((($applicant['data']->status==0)&&($job_status==0)) || (($applicant['data']->status==0)&&($job_status==2)) )
  <a data-school-id="{{ $applicant['data']->school_id }}" data-job-id="{{ $applicant['data']->job_id }}" data-teacher-id="{{ $applicant['data']->teacher_id }}" class="hire RoundBorder DinBold" href="javascript:void(0);" data-hire-status="{{ $applicant['data']->status }}" >Hire</a>
  @elseif(($applicant['data']->status==1))
  <a class="hired RoundBorder DinBold" href="javascript:void(0);">Verifying</a>
  @elseif(($applicant['data']->status==2))
  <a class="hired RoundBorder DinBold" href="javascript:void(0);">Not Available</a>
  @elseif(($applicant['data']->status==3)&&($job_status==3))
  <a class="hired RoundBorder DinBold" href="javascript:void(0);">Confirmed</a>
  @elseif(($applicant['data']->status==4)&&($job_status==4))
  <a class="hired RoundBorder DinBold" href="javascript:void(0);">Not Available</a>
  @else
  <a class="go-disabled-btns for-hire RoundBorder DinBold" href="javascript:void(0);">Hire</a>
  @endif
  
  
  
  @if(($applicant['data']->status == 0))
  <a data-flag="0" data-school-id="{{ $applicant['data']->school_id }}" data-job-id="{{ $applicant['data']->job_id }}" data-teacher-id="{{ $applicant['data']->teacher_id }}" class="remove RoundBorder DinBold" data-block="0" href="javascript:void(0);">Block</a>
  @elseif(($applicant['data']->status == 2))
  <a data-flag="0" data-school-id="{{ $applicant['data']->school_id }}" data-job-id="{{ $applicant['data']->job_id }}" data-teacher-id="{{ $applicant['data']->teacher_id }}" class="remove RoundBorder DinBold" data-block="0" href="javascript:void(0);">Block</a>
  @elseif(($applicant['data']->status == 4))
  <a data-flag="0" data-school-id="{{ $applicant['data']->school_id }}" data-job-id="{{ $applicant['data']->job_id }}" data-teacher-id="{{ $applicant['data']->teacher_id }}" class="remove RoundBorder DinBold" data-block="0" href="javascript:void(0);">Block</a>
  @endif 
       
        <!--<span class="RoundBorder DinBold disabled go-disabled-btns">Blocked</span>--> 
        
      </div>
  <div class="clearfix"></div>
</div>
@endforeach
@endif