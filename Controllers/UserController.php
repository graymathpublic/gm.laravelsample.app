<?php

class UserController extends BaseController {

    public function create_user() {

        $validate = User::validate(Input::all(), 'create_user');

        if ($validate->fails()) {
            return "All fields required";
        } else {

            $email = Input::get('email');
            $am = Member::where("email", "LIKE", "%$email%")->count();

            if ($am == 0) {
                $code = $this->guid();
                $m = new Member;

                $m->email = Input::get('email');
                $m->password = Hash::make(Input::get('password'));
                $m->system_role_id = 2;
                $m->password_reset_token = $code;
                $m->save();

                if ($m->id != "") {

                    $u = new User;

                    $u->id = $m->id;
                    $u->sir_name = '';
                    $u->searched_address = Input::get('searched_address');
                    $u->travel_distance = Input::get('travel_distance');

                    $u->save();

                    // enter user record in user settings table
                    $us = new UserSetting;
                    $us->user_id = $m->id;
                    $us->save();

                    Auth::loginUsingId($m->id);
                    Session::put('user_id', $m->id);
                    Session::put('role', 2);
                    $baseUrl = URL::to('/');
                    $get_template = EmailTemplate::where('template_key', '=', 'new-user-resgister')->first();

                    if ($get_template != null) {
                        $body = str_replace('{url}', $baseUrl, $get_template->body);
                        $body = str_replace('{Name}', $email, $body); // new line add because it not show
                        $subject = $get_template->subject;

                        $parameters = array(
                            'to' => $email,
                            'to_name' => 'No Name',
                            'from' => $this->get_from_email('team'),
                            'from_name' => 'GoTemping Team',
                            'token' => $this->guid(),
                            'body' => $body,
                            'subject' => $subject,
                        );
                        $this->send_notification($parameters, true);
                    }


                    $get_template = EmailTemplate::where('template_key', '=', 'verify-your-email-address')->first();

                    if ($get_template != null) {
                        $body = str_replace('{Name}', $email, $get_template->body);
                        $body = str_replace('{code}', $code, $body);
                        $body = str_replace('{url}', $baseUrl, $body);
                        $subject = $get_template->subject;

                        $parameters = array(
                            'to' => $email,
                            'to_name' => 'No Name',
                            'from' => $this->get_from_email('support'),
                            'from_name' => 'GoTemping Support',
                            'token' => $code,
                            'body' => $body,
                            'subject' => $subject,
                        );
                        $this->send_notification($parameters);
                    }


                    return "true";
                }
            } else {
                return "Email already exist";
            }
        }
    }

    public function email_verification($id) {

        $member = Member::where('password_reset_token', '=', $id)
                        ->where('updated_at', '>=', time() - (24 * 60 * 60))->first();

        if ($member != null) {

            if ($member->email_verification_status == 0) {

                $member->password_reset_token = '';
                $member->email_verification_status = 1;
                $member->save();

                return Redirect::to('userdetail');
            }
        } else {
            return Redirect::to('login');
        }
    }

    public function login() {

        $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

        if (Auth::attempt($userdata, Input::has('keep_me_logged_in'))) {

            $id = Auth::id();

            $m = Member::find($id);

            if ($m->system_role_id == 2) {

                $u = User::find($id);

                $name = ($u->first_name != '') ? $u->first_name : '';

                Session::put('user_name', $name);
                Session::put('user_sur_name', $u->sir_name);
                Session::put('profile_id', $u->profile_file_id);

                if ($u->status == 2) {
                    $redirect_url = URL::to('profile');
                } else {
                    $redirect_url = URL::to('userdetail');
                }
            } else if ($m->system_role_id == 3) {

                $s = School::find($id);

                $name = ($s->name != '') ? $s->name : '';


                if ($s->profile_save_status == 1 && $s->status == 1) {

                    Session::put('school_name', $name);
                    Session::put('logo_id', $s->logo_file_id);
                    $redirect_url = URL::to('school2');
                } else if ($s->profile_save_status == 0 && $s->status == 1) {

                    Session::put('school_name', $name);
                    Session::put('logo_id', $s->logo_file_id);

                    $redirect_url = 'sfirst';
                } else {
                    return $redirect_url = 'not_approved';
                }
            } else if ($m->system_role_id == 5) {

                /*                 * ****************************************************************************************** */
                $s = Agency::find($id);
                $name = ($s->name != '') ? $s->name : '';

                // return $redirect_url = 'sfirst';

                if (($s->profile_save_status == 1 && $s->status == 1) && ( $s->active == 1 )) {

                    Session::put('agency_name', $name);
                    Session::put('logo_id', $s->logo_file_id);
                    Session::put('role', $m->system_role_id);
                    Session::put('user_id', $id);
                    return $redirect_url = URL::to('agency2');
                } else if (($s->profile_save_status == 0 && $s->status == 1 ) && ( $s->active == 1 )) {

                    Session::put('agency_name', $name);
                    Session::put('logo_id', $s->logo_file_id);
                    Session::put('role', $m->system_role_id);
                    Session::put('user_id', $id);
                    return $redirect_url = 'agency_first';
                } else {
                    return $redirect_url = 'not_approved';
                }

                /*                 * ************************************************************************************************* */
            }

            Session::put('role', $m->system_role_id);
            Session::put('user_id', $id);

            return $redirect_url;
        } else {
            return 'false';
        }
    }

    public function logout() {

        Auth::logout();
        Session::forget('user_id');
        Session::forget('user_name');
        Session::forget('school_name');
        Session::forget('role');
        setcookie('closebtn', '');
        setcookie('closeReapproval', '');
        setcookie('pendingApproval', '');
        setcookie('getApproved', '');
        setcookie('closeVerifyEmail', '');

        return Redirect::to('home');
    }

    //Forgot password view
    public function forget_password() {
        //Load view
        return View::make('forget-password')->with('title', 'Forgot Password');
    }

    //Reset Password view
    public function reset_password($token) {

        //Get Username
        $member = Member::where('password_reset_token', '=', $token)
                        ->where('updated_at', '>=', time() - (24 * 60 * 60))->first();

        $response = array();
        if (empty($member)) {
            $flag = 0;
            $message = 'Reset request expire please try again';
        } else {

            $flag = 1;
            $message = $member->email;
        }
        //return $message;
        //Load View
        return View::make('reset-password')->with('title', 'Reset Password')
                        ->with('flag', $flag)
                        ->with('token', $token)
                        ->with('message', $message);
    }

    //Send Reset Link via email
    public function send_password() {
        //Validate input
        $validate = User::validate(Input::all(), 'email_rules');
        $email = Input::get('email');

        if ($validate->fails()) {
            $response = array(
                'flag' => 0,
                'message' => 'Invalid email address'
            );
        } else {
            $member = Member::where('email', '=', $email)->first();
            if (count($member) == 0) {
                $response = array(
                    'flag' => 0,
                    'message' => 'Email address not exits'
                );
            } else {

                $reset_token = $this->guid();
                $member = Member::where('email', '=', $email)->first();
                $member->password_reset_token = $reset_token;
                $member->save();

                $user = User::where('id', '=', $member->id)->first();
                if ((isset($user->first_name)) && (isset($user->sir_name))) {
                    $user_name = $user->first_name . " " . $user->sir_name;
                } else {
                    $user_name = 'User';
                }


                //return $this->get_from_email('noreply');
                $baseUrl = URL::to('/');
                $get_template = EmailTemplate::where('template_key', '=', 'password-reset')->first();
                $body = str_replace('{Name}', $user_name, $get_template->body);
                $body = str_replace('{reset-code}', $reset_token, $body);
                $body = str_replace('{url}', $baseUrl, $body);

                $subject = $get_template->subject;

                $parameters = array(
                    'to' => $email,
                    'to_name' => $user_name,
                    'from' => $this->get_from_email('noreply'),
                    'from_name' => 'Gotemping',
                    'token' => $reset_token,
                    'body' => $body,
                    'subject' => $subject,
                );

                $this->send_notification($parameters);

                $response = array(
                    'flag' => 1,
                    'message' => 'A link to reset your password has been sent to "' . $email . '".'
                );
            }
        }
        return Response::json($response);
    }

    //Reset Password saved
    public function save_password() {

        //Grap input
        $new_password = Input::get('new_password');
        $confirm_password = Input::get('confirm_password');
        $token = Input::get('token');
        $response = array();

        $member = Member::where('password_reset_token', '=', $token)
                        ->where('updated_at', '>=', time() - (24 * 60 * 60))->first();

        if (($new_password == $confirm_password) && ($new_password != '')) {
            if (empty($member)) {
                $response = array(
                    'flag' => 0,
                    'mesage' => 'Reset request expire'
                );
            } else {
                $member->password = Hash::make($new_password);
                $member->password_reset_token = '';
                $member->save();

                $response = array(
                    'flag' => 1,
                    'mesage' => 'Password Update successfully...'
                );
            }
        } else {
            $response = array(
                'flag' => 0,
                'mesage' => 'Password and confirm password mismatch try again...'
            );
        }
        return Response::json($response);
    }

}
