<?php

class RemotingController extends BaseController {

    protected $schoolid, $teacher_id;
    protected $response = array();

    public function __construct() {
        
    }

    public function auth() {

        //Grab data from device 
        $device_type = Input::get('device_type');
        $device_id = Input::get('device_id');
        $user_id = Input::get('user_id');
        $token = Input::get('token');

        //Get Member ID
        $login = MemberMobileLogin::where('login_token', '=', $token)
                ->where('device_id', '=', $device_id)
                ->where('member_id', '=', $user_id)
                ->where('active', '=', 1)
                ->first();
        if (count($login) == 0) {
            $this->response = array(
                'success' => false,
                'message' => 'Token has been expired'
            );

            //Return Response
            return Response::json($this->response);
        } else {
            return $this->teacher_id = $login->member_id;
        }
    }

    public function auth2() {

        //Grab data from device 
        $device_type = Input::get('device_type');
        $device_id = Input::get('device_id');
        $user_id = Input::get('user_id');
        $token = Input::get('token');

        //Get Member ID
        $login = MemberMobileLogin::where('login_token', '=', $token)
                ->where('device_id', '=', $device_id)
                ->where('member_id', '=', $user_id)
                ->where('active', '=', 1)
                ->first();
        return count($login) != 0;
    }

    //Activity Stream
    public function activity_stream() {

        if (!$this->auth2()) {
            return Response::json(array('OK' => false));
        }

        $this->teacher_id = Input::get('user_id');

        $jobs = ViewActivityStream::where('teacher_id', '=', $this->teacher_id)
                        ->orderBy('job_id', 'DESC')->get();

        $total_jobs = count($jobs);

        if ($total_jobs == 0) {
            $this->response = array(
                'OK' => true,
                'success' => false,
                'count' => $total_jobs,
                'data' => NULL,
            );
        } else {

            $data = array();
            $log_file = 'assets/images/temp-school.jpg';
            $position = '';

            foreach ($jobs as $job) {
                $log_file = 'assets/images/temp-school.jpg';
                if ($job->logo_id != null) {
                    $log_file = URL::to('school_download', array($job->logo_id, true));
                }

                $position = $job->class . ', ' . $job->subject;
                $position = trim($position, ', ');

                $data [] = array(
                    'content' => $job,
                    'logo' => $log_file,
                    'position' => $position
                );
            }

            $this->response = array(
                'OK' => true,
                'success' => true,
                'count' => $total_jobs,
                'data' => $data
            );
        }
        //Return Response
        return Response::json($this->response);
    }

    //Availablity
    public function availability() {

        if (!$this->auth2()) {
            return Response::json(array('OK' => false));
        }

        $this->teacher_id = Input::get('user_id');

        //Get data of jobs
        $jobs = ViewRestTeacherBooking::where('teacher_id', '=', $this->teacher_id)
                        ->orderBy('job_id', 'DESC')->get();
        $total_jobs = count($jobs);

        if ($total_jobs == 0) {
            $this->response = array(
                'OK' => true,
                'success' => false,
                'count' => $total_jobs,
                'data' => 'NULL'
            );
        } else {

            $data = array();
            $log_file = 'assets/images/temp-school.jpg';
            $position = '';
            foreach ($jobs as $job) {
                $log_file = 'assets/images/temp-school.jpg';

                if ($job->logo_id != null) {
                    $log_file = URL::to('school_download', array($job->logo_id, true));
                }

                $position = $job->class . ', ' . $job->subject;
                $position = trim($position, ', ');

                $data [] = array(
                    'content' => $job,
                    'logo' => $log_file,
                    'position' => $position
                );
            }
            $this->response = array(
                'OK' => true,
                'success' => true,
                'count' => $total_jobs,
                'data' => $data
            );
        }
        //Return Response
        return Response::json($this->response);
    }

    //Job Detail
    public function job_detail() {

        if (!$this->auth2()) {
            return Response::json(array('OK' => false));
        }

        $job_id = Input::get('job_id');
        $this->teacher_id = Input::get('user_id');

        //Get data of jobs
        $jobs = ViewActivityStream::where('teacher_id', '=', $this->teacher_id)
                        ->where('job_id', '=', $job_id)->get();

        $total_jobs = count($jobs);

        if ($total_jobs == 0) {
            $this->response = array(
                'OK' => true,
                'success' => false,
                'data' => '401'
            );
        } else {

            $data = array();
            $log_file = 'assets/images/temp-school.jpg';
            $message_attachment = null;
            $position = '';
            foreach ($jobs as $job) {

                if ($job->logo_id != null) {
                    $log_file = URL::to('school_download', array($job->logo_id, true));
                }

                if ($job->sm_message_attachment != null) {
                    $message_attachment = URL::to('school_download', array($job->sm_message_attachment, true));
                    if (!file_exists($message_attachment)) {
                        $message_attachment = null;
                    }
                } else {
                    $message_attachment = null;
                }

                $position = $job->class . ', ' . $job->subject;
                $position = trim($position, ', ');

                $data [] = array(
                    'content' => $job,
                    'logo' => $log_file,
                    'attachment' => $message_attachment,
                    'position' => $position
                );
            }

            $this->response = array(
                'OK' => true,
                'success' => true,
                'data' => $data
            );
        }
        //Return Response
        return Response::json($this->response);
    }

    //Job Apply
    public function apply_job() {

        if (!$this->auth2()) {
            return Response::json(array('OK' => false));
        }

        $job_id = Input::get('job_id');
        $school_id = Input::get('school_id');
        $this->teacher_id = Input::get('user_id');
        $teacher_id = $this->teacher_id;

        $response = new JobResponse;
        $response->job_id = $job_id;
        $response->teacher_id = $teacher_id;
        $response->school_id = $school_id;
        $response->save();

        if ($response->id == null) {
            $this->response = array(
                'OK' => true,
                'success' => false,
                'data' => '401'
            );
        } else {
            $this->response = array(
                'OK' => true,
                'success' => true,
                'data' => 'Applied'
            );
        }
        //Return Response
        return Response::json($this->response);
    }

    public function job_action() {

        if (!$this->auth2()) {
            return Response::json(array('OK' => false));
        }

        $job_id = Input::get('job_id');
        $this->teacher_id = Input::get('user_id');
        $action = Input::get('action');



        $j = Job::find($job_id);
        if ($j != null) {

            $jr = JobResponse::where('teacher_id', '=', $this->teacher_id)->where('job_id', '=', $job_id)->first();

            if (( (is_numeric($action)) && ($action >= 0) && ($action <= 4)) && (count($jr) != 0)) {

                if ($action == 3) {
                    $j->status = 1;
                    $j->save();

                    $start_date = strtotime($j->start_date);
                    $start_date = date('m/d/Y', $start_date);
                    $end_date = strtotime($j->end_date);
                    $end_date = date('m/d/Y', $end_date);

                    $query = 'SELECT *
                           FROM `vw_activity_streem`
                           WHERE ( (to_date BETWEEN "' . $start_date . '" AND "' . $end_date . '") OR 
                           (from_date BETWEEN "' . $start_date . '" AND "' . $end_date . '") OR
                           (to_date >= "' . $start_date . '" AND from_date <= "' . $end_date . '" )) AND 
                           teacher_id = ' . $this->teacher_id . ' AND job_id != ' . $job_id;

                    $jobs = DB::select(DB::raw($query));

                    if (count($jobs) > 0) {
                        foreach ($jobs as $job) {

                            $newRes = JobResponse::where('teacher_id', '=', $job->teacher_id)->where('job_id', '=', $job->job_id)->first();
                            if (empty($newRes)) {
                                $jrnew = new JobResponse;
                                $jrnew->job_id = $job->job_id;
                                $jrnew->teacher_id = $job->teacher_id;
                                $jrnew->school_id = $job->school_id;
                                $jrnew->status = 2;
                                $jrnew->save();
                            } else {
                                $newRes->status = 2;
                                $newRes->save();
                            }
                        }
                    }

                    $booking = new Booking;
                    $booking->job_id = $job_id;
                    $booking->teacher_id = $this->teacher_id;
                    $booking->school_id = $j->school_id;
                    $booking->agency_id = $j->agency_id;
                    $booking->save();
                }
                $jr->status = $action;
                $jr->save();

                $this->response = array(
                    'OK' => true,
                    'success' => true,
                    'message' => 'Action performed'
                );
            } else {
                $this->response = array(
                    'OK' => true,
                    'success' => false,
                    'message' => 'Invaild action'
                );
            }
        } else {
            $this->response = array(
                'OK' => true,
                'success' => false,
                'message' => 'No job found'
            );
        }
        //Return Response
        return Response::json($this->response);
    }

    public function register_device_pn() {

        if (!$this->auth2()) {
            return Response::json(array('OK' => false));
        }

        //Grab data from device 
        $device_type = Input::get('device_type');
        $device_id = Input::get('device_id');
        $user_id = Input::get('user_id');
        $token = Input::get('token');
        $pin = Input::get('pin');

        //Get Member ID
        $login = MemberMobileLogin::where('login_token', '=', $token)->first();

        if ($login->pn_id != $pin) {
            $login->pn_id = $pin;
            $login->save();
            return Response::json(array('OK' => true, 'result' => true));
        }

        return Response::json(array('OK' => true, 'result' => false));
    }

}
