<?php

class JobController extends BaseController {

    protected $schoolid, $saCount;
    protected $attributes_detail = array();

    public function __construct() {
        $this->schoolid = $this->current_user();
    }

    public function getUserAttributes($id) {
        $schoolAttributes = $this->getSchoolAttribute($id);
        $this->saCount = $schoolAttributes['saCount'];
        $this->attributes_detail = $schoolAttributes['attributes_detail'];
        $this->languages = Language::orderBy('title', 'ASC')->get();
        $attributes = View::make('school-post-job-attribute')->with('attributes_detail', $this->attributes_detail)->with('saCount', $this->saCount)->with('languages', $this->languages)->render();
        $schoolData = SchoolMessage::where('school_id', '=', $id)->where('active', '=', 1)->first(array('start_time', 'finish_time'));

        $data = array(
            'times' => $schoolData,
            'attributes' => $attributes,
        );
        return Response::json($data);
    }

    public function showPostJob() {
        $category_id = 1;
        $isAgency = false;
        $regSchools = array();

        if (Session::get('role') == 5) {
            $isAgency = true;
            $currentUser = $this->current_user();
            $schools = DB::table('schools')
                    ->leftJoin('agency_schools', 'schools.id', '=', 'agency_schools.schools_id')
                    ->where('agency_schools.agencies_id', '=', $currentUser)
                    ->select('schools.id', 'schools.name')
                    ->get();

            $regSchools[''] = 'Select school';
            if (count($schools) > 0) {
                foreach ($schools as $school) {
                    $regSchools[$school->id] = $school->name;
                }
            } else {
                $regSchools[''] = 'Select school';
            }
        }

        $schoolAttributes = $this->getSchoolAttribute($this->schoolid);
        $this->saCount = $schoolAttributes['saCount'];
        $this->attributes_detail = $schoolAttributes['attributes_detail'];

        $getSchoolMessage = SchoolMessage::where('school_id', '=', $this->schoolid)->get(array('start_time'));
        if (count($getSchoolMessage) == 1) {
            $getSchoolMessage = ($getSchoolMessage[0]->start_time == '' OR $getSchoolMessage[0]->start_time == NULL) ? '' : $getSchoolMessage[0]->start_time;
        } else {
            $getSchoolMessage = '';
        }

        $sm = SchoolMessage::where('school_id', '=', $this->schoolid)->first();  //$sm = SchoolMessage::find($this->current_user());
        $finish_time = '';
        if ($sm != null) {
            $finish_time = $sm->finish_time;
        }

        $reason = array(
            '' => 'Select reason',
            'Personal Leave' => 'Personal Leave',
            'Annual Leave' => 'Annual Leave',
            'Long Service Leave' => 'Long Service Leave',
            'Sick Leave' => 'Sick Leave',
            'Carers Leave' => 'Carers Leave',
            'Curriculum Day' => 'Curriculum Day',
            'Teacher Professional Development' => 'Teacher Professional Development',
            'Parents Meeting' => 'Parents Meeting',
            'Report Writing' => 'Report Writing',
            'Camp' => 'Camp',
            'Other' => 'Other'
        );


        $this->languages = Language::orderBy('title', 'ASC')->get();

        return View::make('school-post-job')->with('title', 'Post Job ')
                        ->with('attributes_detail', $this->attributes_detail)
                        ->with('reason', $reason)
                        ->with('saCount', $this->saCount)
                        ->with('start_time', $getSchoolMessage)
                        ->with('finish_time', $finish_time)
                        ->with('isAgency', $isAgency)
                        ->with('regSchools', $regSchools)
                        ->with('languages', $this->languages);
    }

    public function post_job_save() {

        $validate = Job::validate(Input::all(), 'job_save');
        if ($validate->fails()) {
            return "false";
        } else {

            $attributes = Input::get('mattributes');

            $j = new Job;

            $j->school_id = $this->schoolid;
            if (Session::get('role') == 5) {
                $agency_id = $j->school_id;

                $j->school_id = Input::get('school');
                $j->agency_id = $agency_id;
            }

            $j->teacher_replacement = Input::get('teacher_replacement');
            $j->start_date = (Input::get('start_date') == '') ? '0000-00-00' : date('Y-m-d', strtotime(str_replace('/', '-', Input::get('start_date'))));
            $j->end_date = (Input::get('end_date') == '') ? '0000-00-00' : date('Y-m-d', strtotime(str_replace('/', '-', Input::get('end_date'))));
            $j->exclude_start_date = (Input::get('exclude_start_date') == '') ? '0000-00-00' : date('Y-m-d', strtotime(str_replace('/', '-', Input::get('exclude_start_date'))));
            $j->exclude_end_date = (Input::get('exclude_end_date') == '') ? '0000-00-00' : date('Y-m-d', strtotime(str_replace('/', '-', Input::get('exclude_end_date'))));
            $j->exclude_holidays = Input::has('exclude_holidays') ? 1 : 0;
            $j->start_time = Input::get('start_time');
            $j->finish_time = Input::get('finish_time');
            $j->additional_comment = Input::get('additional_comment');
            $j->have_fir_certificate = Input::has('have_fir_certificate') ? 1 : 0;
            $j->have_mini_bus_licence = Input::has('have_mini_bus_licence') ? 1 : 0;
            $j->absence_reason = Input::get('reason');
            $j->have_anaphylaxis_certificate = Input::has('have_anaphylaxis_certificates') ? 1 : 0;
            $j->active = 1;

            $j->save();

            if ($j->id != '') {

                if (Input::get('position') != '') {
                    $ag = Attribute::find(Input::get('position'));
                } elseif (Input::get('subject') != '') {
                    $ag = Attribute::find(Input::get('subject'));
                }

                if (Input::has('position') != '') {
                    $ja = new JobAttribute;
                    $ja->job_id = $j->id;
                    $ja->attribute_id = (Input::has('position')) ? Input::get('position') : NULL;
                    $ja->attribute_group_id = $ag->attribute_group_id;
                    $ja->attribute_type = 's';
                    $ja->save();
                }

                if (Input::has('subject') != '') {
                    $ja = new JobAttribute;
                    $ja->job_id = $j->id;
                    $ja->attribute_id = (Input::has('subject')) ? Input::get('subject') : NULL;
                    $ja->attribute_group_id = $ag->attribute_group_id;
                    $ja->attribute_type = 's';
                    $ja->save();
                }

                if (is_array($attributes)) {

                    foreach ($attributes as $key => $val) {

                        $ag = Attribute::find($val);

                        $ja = new JobAttribute;
                        $ja->job_id = $j->id;
                        $ja->attribute_id = $val;
                        $ja->attribute_group_id = $ag->attribute_group_id;
                        $ja->attribute_type = 'm';

                        $ja->save();
                    }
                }
            }

            if ($j->id != '') {

                //Get Job Option ID
//              $postiton_id = JobAttribute::where('job_id', '=', $j->id)->where('attribute_type', '=', 's')->first()->attribute_id;
                $postiton_id = Input::get('position');
                $subject_id = Input::get('subject');

                //Get School ID from Sesstion
                $school_id = $j->school_id;
                //Find School by School ID
                $school = School::find($school_id);
                //Find School LAT
                $school_lat = $school->g_lat;
                //Find School LONG
                $school_long = $school->g_long;
//                if ($school->g_lat != null) {
                //Start Date of Job
                $job_start_date = str_replace('/', '-', Input::get('start_date'));
                //End Date of Job
                $job_end_date = str_replace('/', '-', Input::get('end_date'));

                //add entry in notification table

                $parameters = array(
                    'start_date' => $job_start_date,
                    'end_date' => $job_end_date,
                    'postiton_id' => $postiton_id,
                    'subject_id' => $subject_id,
                    'lat' => $school_lat,
                    'long' => $school_long,
                    'school_id' => $school_id,
                    'notification_type' => 'new_job',
                    'job_id' => $j->id,
                );

                $this->save_notification($parameters);
//                } // end of if $school->g_lat check;
            }
            return 'true';
        }
    }

    //Get all available jobs
    public function school_available_jobs() {
        //Get all records by school id which is visible=1 and Status 1

        $jobColum = 'school_id';
        if (Session::get('role') == 5) {
            //Get data of jobs
            $jobs = ViewOpeningJob::where('agency_id', '=', $this->schoolid)
                    ->Where('visible', '=', 1)->Where('active', '=', 1)
                    ->groupBy('job_id')->orderBy('job_id', 'DESC')
                    ->get();
        } else {

            //Get data of jobs
            $jobs = ViewOpeningJob::where($jobColum, '=', $this->schoolid)
                    ->Where('visible', '=', 1)
                    ->Where('active', '=', 1)
                    ->whereNull('agency_id')
                    ->groupBy('job_id')->orderBy('job_id', 'DESC')
                    ->get();
        }


        $status = $position = '';
        $data = array();

        foreach ($jobs as $job) {

            $job_info = JobAttribute::where('job_id', '=', $job->job_id)->first();

            $aa = DB::table('attribute_groups as a')->select('a.*')
                            ->join('attribute_groups as b', 'a.id', '=', 'b.parent_id')
                            ->where('b.id', '=', $job_info->attribute_group_id)->first();
            $level = '';
            if (count($aa) > 0) {
                $level = $aa->name_for_school . ', ';
            }

            //Job Status
            $status = $this->job_response_status($job->job_id);

            //Job Position
            $position = $job->class . ', ' . $job->subject;
            $position = trim($position, ', ');

            $data[] = array(
                'job_id' => $job->job_id,
                'job_position' => $level . $position,
                'job_status' => $status,
                'from_date' => $job->from_date,
                'to_date' => $job->to_date,
                'time' => $job->time,
                'finish_time' => $job->finish_time,
                'school_name' => $job->school_name,
            );
        }
        return Response::json($data);
    }

    //Get Job Response Status
    public function job_response_status($id) {


        if (Session::get('role') == 5) {
            $school_id = Job::find($id);
            $this->schoolid = $school_id->school_id;
        }

        $jobs = JobResponse::where('job_id', '=', $id)->where('blocked', '=', 1)->where('school_id', '=', $this->schoolid)->get();
        $status = '';
        $data = array();

        foreach ($jobs as $job) {
            $data[] = $job->status;
        }
        if (empty($data)) {
            return "Searching";
        } else {
            if (max($data) == 3) {
                return "Confirmed";
            } else {
                if (in_array('1', $data)) {
                    return "Verifying";
                } elseif (in_array('2', $data)) {
                    return "Choose Again";
                } elseif (in_array('3', $data)) {
                    return "Confirmed";
                } elseif (in_array('4', $data)) {
                    return "Choose Again";
                } else {
                    return "[" . count($jobs) . "]";
                }
            }
        }
    }

    //Clear all confirm jobs
    public function school_clear_confirm_jobs() {

        //Get colums from db
        $colums = array('id');
        $col = 'school_id';

        if (Session::get('role') == 5) {
            $col = 'agency_id';
        }

        //Get all records by school id and Status 1
        $jobs = Job::whereRaw('status = 1 and ' . $col . ' = ' . $this->schoolid)->get($colums);
        if (count($jobs) > 0) {
            foreach ($jobs as $job) {
                $id = $job->id;
                $update_colum = array('is_visible' => 0);
                $job = Job::whereRaw('id = ' . $id . ' and ' . $col . ' = ' . $this->schoolid)->update($update_colum);
            }
            $response = array('response' => 'All confirm jobs are cleared...');
        } else {
            $response = array('response' => 'No confirm job found...');
        }
        return Response::json($response);
    }

    //Cancel jobs
    public function school_cancel_jobs() {
        $jobs = Input::get('job');
        $col = 'school_id';

        if (Session::get('role') == 5) {
            $col = 'agency_id';
        }

        if (count($jobs) > 0) {

            foreach ($jobs as $job) {
                $id = $job;
                $update_colum = array('is_visible' => 2);
                $job = Job::whereRaw('id = ' . $id . ' and ' . $col . ' = ' . $this->schoolid)->update($update_colum);
            }

            //return $teachers;
            $response = array('Good' => true, 'response' => 'All jobs are canceled...');
        } else {
            $response = array('Good' => false, 'response' => 'No job found...');
        }
        return Response::json($response);
    }

    //Get all applied teachers
    public function get_applied_teacher() {

        //Initilaiz data array
        $data = array();
        $jobColum = 'school_id';
        if (Session::get('role') == 5) {
            $jobColum = 'agency_id';
        }

        //Grab Values
        $job_id = Input::get('job_id');
        $filter = strtolower(Input::get('filter'));
        $filter = strtolower($filter);
        $job_state = 0;

        $job_state = DB::select(DB::raw("CALL getJobStatus($job_id)"));
        $job_state = $job_state[0]->find_status;

        //Get data of jobs
        if ($filter == 'all') {
            $applicants = ViewJob::where($jobColum, '=', $this->schoolid)
                            ->Where('job_id', '=', $job_id)
                            ->Where('blocked', '=', 'No')
                            ->groupBy('teacher_id')->get();
        } elseif ($filter == 'yes' || $filter == 'no') {
            $applicants = ViewJob::where($jobColum, '=', $this->schoolid)
                            ->Where('job_id', '=', $job_id)
                            ->Where('blocked', '=', 'NO')
                            ->Where('taught_here_before', '=', $filter)
                            ->groupBy('teacher_id')->get();
        } elseif ($filter == 'rating') {
            $applicants = ViewJob::where($jobColum, '=', $this->schoolid)
                            ->Where('job_id', '=', $job_id)
                            ->Where('blocked', '=', 'NO')
                            ->whereNotNull('rating')
                            ->groupBy('teacher_id')->get();
        } elseif ($filter == 'favourtie') {
            $applicants = ViewJob::where($jobColum, '=', $this->schoolid)
                            ->Where('job_id', '=', $job_id)
                            ->Where('blocked', '=', 'NO')
                            ->Where('favorite', '=', 'Yes')
                            ->groupBy('teacher_id')->get();
        } elseif ($filter == 'years_of_experiance') {
            $applicants = ViewJob::where($jobColum, '=', $this->schoolid)
                            ->Where('job_id', '=', $job_id)
                            ->Where('blocked', '=', 'NO')
                            ->Where('is_experienced', '=', 'Yes')
                            ->groupBy('teacher_id')->get();
        } else {
            $applicants = ViewJob::where($jobColum, '=', $this->schoolid)
                            ->Where('job_id', '=', $job_id)
                            ->Where('blocked', '=', 'No')
                            ->groupBy('teacher_id')->get();
        }

        //Papulate data array with applicant and distance
        foreach ($applicants as $applicant) {

            //School Lat Long
            $school_lat = $applicant->school_lat;
            $school_long = $applicant->school_long;

            //Teacher Lat Long
            $teacher_lat = $applicant->teacher_lat;
            $teacher_long = $applicant->teacher_long;

            if (($school_lat != '' && $school_long != '') && ($teacher_lat != '' && $teacher_long != '')) {

                //Calculate distance from teacher and school lat lang
                $distance = $this->distance($teacher_lat, $teacher_long, $school_lat, $school_long, 'k');
            } else {

                //Calculate distance from teacher and school lat lang
                $distance = 'N/A';
            }

            //Round distance value from point to real number
            if ($distance != 'N/A') {
                $distance = round($distance) . ' km';
            }
            $experience = $applicant->experience;
            $data[] = array('data' => $applicant, 'distance' => $distance, 'experience' => $experience);
        }

        //Call view and pass data of applicant
        return View::make('school-applied-teacher-response')
                        ->with('applicants', $data)
                        ->with('job_status', $job_state);
    }

    public function get_job_detail() {
        //Grab Values
        $job_id = Input::get('job_id');
        $position = '';
        $data = array();
        $log_file = 'null';

        //Get data of jobs
        $jobs = ViewGetJobDetail::where('job_id', '=', $job_id)->first();

        $total_jobs = count($jobs);

        if ($total_jobs == 0) {
            $response = array(
                'success' => false,
                'data' => '401'
            );
        } else {

            if ($jobs->logo_id != null) {
                $log_file = URL::to('school_download', array($jobs->logo_id, true));
            }

            $position = $jobs->class . ', ' . $jobs->subject;
            $position = trim($position, ', ');

            $data [] = array(
                'content' => $jobs,
                'logo' => $log_file,
                'position' => $position
            );
        }
        //Return Response
        return View::make('school-job-detail')
                        ->with('position', $position)
                        ->with('name', $jobs->school_name)
                        ->with('logo', $log_file)
                        ->with('date', $jobs->date)
                        ->with('address', $jobs->school_address)
                        ->with('from_date', $jobs->from_date)
                        ->with('to_date', $jobs->to_date)
                        ->with('start_time', $jobs->start_time)
                        ->with('additional_comment', $jobs->additional_comment);
    }

    //Get all applied teachers
    public function school_teacher_favourite() {

        //Grab Values
        $flag = Input::get('flag');
        $teacher_id = Input::get('teacher_id');
        $school_id = Input::get('school_id');

        if ($flag == 'No') {
            $flag = 1;
            //Add entry in SchoolTeacherFilter as favourite
            $teacher_fav = new SchoolTeacherFilter;
            $teacher_fav->school_id = $school_id;
            $teacher_fav->teacher_id = $teacher_id;
            $teacher_fav->entry_type = $flag;
            $teacher_fav->save();
        } else {
            //Delete entry from SchoolTeacherFilter
            $teacher_fav = SchoolTeacherFilter::where('school_id', '=', $school_id)
                    ->where('teacher_id', '=', $teacher_id)
                    ->where('entry_type', '=', '1');
            $teacher_fav->delete();
        }
        /* $update_colum = array('entry_type'=>$flag);
          $fav = SchoolTeacherFilter::whereRaw('school_id = '.$school_id.' and teacher_id='.$teacher_id)->update($update_colum); */
        return Response::json($response = array('flag' => true));
    }

    //Block/Unblock School Teacher
    public function school_teacher_block() {

        //Grab Values
        $flag = Input::get('flag');
        $teacher_id = Input::get('teacher_id');
        $school_id = Input::get('school_id');

        //Update blocked Colum
        $update_colum = array('blocked' => $flag);
        //Execute query for table JobResponse
        $job_response = JobResponse::whereRaw('school_id = ' . $school_id . ' and teacher_id=' . $teacher_id)->update($update_colum);

        //If flag==0 teacher will block else flag==1 teacher will unblock
        if ($flag == 0) {
            //Block/Unblock the teacher
            $teacher_filter = new SchoolTeacherFilter;
            $teacher_filter->school_id = $school_id;
            $teacher_filter->teacher_id = $teacher_id;
            $teacher_filter->entry_type = $flag;
            $teacher_filter->save();
        } elseif ($flag == 1) {
            //Delete entry from SchoolTeacherFilter
            $teacher_fav = SchoolTeacherFilter::where('school_id', '=', $school_id)
                    ->where('teacher_id', '=', $teacher_id)
                    ->where('entry_type', '=', '0');
            $teacher_fav->delete();
        }

        //Return Response
        return Response::json($response = array('flag' => true));
    }

    //Get all applied teachers
    public function school_teacher_hire_action() {

        //Grab Values
        $flag = Input::get('flag');
        $teacher_id = Input::get('teacher_id');
        $school_id = Input::get('school_id');
        $job_id = Input::get('job_id');

        //Update blocked Colum
        $update_colum = array('status' => $flag + 1);
        //Execute query for table JobResponse
        $job_response = JobResponse::whereRaw('school_id = ' . $school_id . ' and teacher_id=' . $teacher_id . ' and job_id=' . $job_id)->update($update_colum);

        //Block/Unblock the teacher
        $teacher_filter = new Notification;
        $teacher_filter->deliver_from_school_id = $school_id;
        $teacher_filter->deliver_to_user_id = $teacher_id;
        $teacher_filter->job_id = $job_id;
        $teacher_filter->notification_type = 'confirm';
        $teacher_filter->save();

        return Response::json($response = array('flag' => true));
    }

    //Get all applied teachers
    public function teacher_actions($type) {

        //Initilaiz data array
        $data = array();
        if ($type == 'favorite') {
            $applicants = ViewJob::where('school_id', '=', $this->schoolid)
                            ->where('favorite', '=', 'Yes')
                            ->where('blocked', '=', 'No')
                            ->groupBy('teacher_id')->get();
        }if ($type == 'blocked') {
            $applicants = ViewJob::where('school_id', '=', $this->schoolid)
                            ->where('blocked', '=', 'Yes')
                            ->groupBy('teacher_id')->get();
        }if ($type == 'taught') {
            $applicants = ViewJob::where('school_id', '=', $this->schoolid)
                            ->where('taught_here_before', '=', 'Yes')
                            ->where('blocked', '=', 'No')
                            ->groupBy('teacher_id')->get();
        }

        //Papulate data array with applicant and distance
        foreach ($applicants as $applicant) {
            //School Lat Long
            $school_lat = $applicant->school_lat;
            $school_long = $applicant->school_long;
            //Teacher Lat Long
            $teacher_lat = $applicant->teacher_lat;
            $teacher_long = $applicant->teacher_long;
            if (($school_lat != '' && $school_long != '') && ($teacher_lat != '' && $teacher_long != '')) {

                //Calculate distance from teacher and school lat lang
                $distance = $this->distance($teacher_lat, $teacher_long, $school_lat, $school_long, 'k');
            } else {

                //Calculate distance from teacher and school lat lang
                $distance = 'N/A';
            }


            if ($distance != 'N/A') {
                $distance = round($distance) . ' km';
            }

            if ($applicant->experience == '') {
                $experience = 'N/A';
            } else {
                $experience = $applicant->experience . ' years';
            }

            $data[] = array('data' => $applicant, 'distance' => $distance, 'experience' => $experience);
        }
        //Call view and pass data of applicant

        return View::make('school-get-all-favourte-teacher')->with('applicants', $data)->with('with_actions', $type);
    }

    public function teacher_filters() {
        $filter = Input::get('filter');
        //Initilaiz data array
        $data = array();

        if ($filter == 'all') {
            $applicants = ViewJob::where('school_id', '=', $this->schoolid)
                            ->Where('blocked', '=', 'Yes')
                            ->groupBy('teacher_id')->get();
        } else {
            $applicants = ViewJob::where('school_id', '=', $this->schoolid)
                            ->Where('blocked', '=', 'Yes')
                            ->Where('taught_here_before', '=', $filter)
                            ->groupBy('teacher_id')->get();
        }

        //Papulate data array with applicant and distance
        foreach ($applicants as $applicant) {

            //School Lat Long
            $school_lat = $applicant->school_lat;
            $school_long = $applicant->school_long;
            //Teacher Lat Long
            $teacher_lat = $applicant->teacher_lat;
            $teacher_long = $applicant->teacher_long;

            if (($school_lat != '' && $school_long != '') && ($teacher_lat != '' && $teacher_long != '')) {

                //Calculate distance from teacher and school lat lang
                $distance = $this->distance($teacher_lat, $teacher_long, $school_lat, $school_long, 'k');
            } else {

                //Calculate distance from teacher and school lat lang
                $distance = 'N/A';
            }


            if ($distance != 'N/A') {
                $distance = round($distance) . ' km';
            }

            $data[] = array('data' => $applicant, 'distance' => $distance, 'experience' => $applicant->experience);
        }
        //Call view and pass data of applicant

        return View::make('school-get-all-favourte-teacher')->with('applicants', $data)->with('with_actions', 'blocked');
    }

    public function who_available() {
        $isAgency = false;
        if (Session::get('role') == 5) {
            $isAgency = true;
        }
        return View::make('whos-available')->with('title', 'who\'s available ')->with('isAgency', $isAgency);
    }

}
